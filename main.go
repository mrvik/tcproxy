package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

const BufSize = 2048

func main() {
	var (
		from, to string
		loglevel int
		color    bool
		wg       sync.WaitGroup
	)

	flag.StringVar(&from, "from", "", "Proxy listen addr")
	flag.StringVar(&to, "to", "", "Proxy target addr")
	flag.IntVar(&loglevel, "level", int(logrus.WarnLevel), "Log level")
	flag.BoolVar(&color, "color", color, "Force colors")
	flag.Parse()

	if from == "" || to == "" {
		logrus.Fatal("from and to parameters are mandatory")
	}

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: color,
	})

	logrus.SetLevel(logrus.Level(loglevel))

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	listener, err := net.Listen("tcp", from)
	if err != nil {
		logrus.Fatalf("start listener: %s", err)
	}

	go func() {
		<-ctx.Done()
		listener.Close()
	}()

outerLoop:
	for {
		conn, err := listener.Accept()
		switch {
		case err == nil:
		case errors.Is(err, net.ErrClosed):
			break outerLoop
		case err != nil:
			logrus.Errorf("Accept connection: %s", err)

			continue
		}

		logrus.WithField("source", conn.RemoteAddr().String()).Infof("New connection")

		wg.Add(1)

		go copyConn(ctx, &wg, to, conn)
	}

	logrus.Infof("Waiting for remaining connections")
	wg.Wait()
}

func copyConn(ctx context.Context, wg *sync.WaitGroup, dest string, src net.Conn) {
	defer wg.Done()
	defer src.Close()

	dst, err := net.Dial("tcp", dest)
	if err != nil {
		logrus.Errorf("Dial target: %s", err)
		return
	}

	go func() {
		<-ctx.Done()

		dst.Close()
		src.Close()
	}()

	eg := errgroup.Group{}

	eg.Go(func() error {
		defer src.Close()
		defer dst.Close()

		var buf [BufSize]byte

		n, err := io.CopyBuffer(dst, src, buf[:])
		if err == nil {
			return nil
		}

		logrus.WithField("direction", "dst<-src").Infof("Copied %d bytes and closed due to: %s", n, err)

		return fmt.Errorf("dst<-src: %w", err)
	})

	eg.Go(func() error {
		defer src.Close()
		defer dst.Close()

		var buf [BufSize]byte

		n, err := io.CopyBuffer(src, dst, buf[:])
		if err == nil {
			return nil
		}

		logrus.WithField("direction", "src<-dst").Infof("Copied %d bytes and closed due to: %s", n, err)

		return fmt.Errorf("src<-dst: %w", err)
	})

	if err := eg.Wait(); err != nil {
		logrus.Infof("Proxy closed: %s", err)
	}
}
